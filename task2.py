

class Fifo:
  def __init__(self):
    self.numbers = []
    self.max = 4

  def add(self, x):
    if len(self.numbers) < self.max:
      self.numbers.append(x)
    else:
      self.numbers.pop(0)
      self.numbers.append(x)


  def remove(self):
    return self.numbers.pop(0)

  def print(self):
    print(self.numbers)


fifa = Fifo()
fifa.add(1)
fifa.print()
fifa.add(2)
fifa.print()
fifa.add(3)
fifa.print()
fifa.add(4)
fifa.print()
fifa.remove()
fifa.print()
fifa.add(2)
fifa.print()
fifa.add(1)
fifa.print()




