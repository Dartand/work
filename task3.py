def Sort(nambers):
  if len(nambers) > 1:
    mesto =  len(nambers) // 2
    pivot = nambers.pop(mesto)
    grtr_lst, equal_lst, smlr_lst = [], [pivot], []
    for item in nambers:
      if item == pivot:
        equal_lst.append(item)
      elif item > pivot:
        grtr_lst.append(item)
      else:
        smlr_lst.append(item)
    return (Sort(smlr_lst) + equal_lst + Sort(grtr_lst))
  else:
    return nambers


y = [2, 3, 4, 1, 6, 9, 5]
x = Sort(y)
print(x)
